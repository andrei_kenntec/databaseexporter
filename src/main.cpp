#include <gtk/gtk.h>
#include <iostream>

#include "include/logging.h"

using namespace std;

GtkBuilder      *builder; 
GtkWidget       *window;
GtkWidget       *bttn_connect_database;
GtkComboBoxText *combo_tableNames;
GtkWidget       *bttn_disconectDB;
GtkWidget       *bttn_exportCSV;
GtkToggleButton *sinceLastExportTggl;
GtkComboBoxText *machineSelector;
GtkEntry        *passwordEntry;
    
extern "C" void on_window_main_destroy();
extern "C" void on_bttn_connect_database_clicked(GtkButton *b);
extern "C" void on_combo_tableNames_changed(GtkComboBoxText *b);
extern "C" void on_bttn_disconectDB_clicked(GtkButton *b);
extern "C" void on_bttn_exportCSV_clicked(GtkButton *b);
extern "C" void on_sinceLastExportTggl_toggled(GtkToggleButton *b);
extern "C" void on_machineSelector_changed(GtkComboBoxText *b);
extern "C" void on_passwordEntry_editing_done(GtkEntry *b);



int main(int argc, char *argv[])
{
    string tableName;

    gtk_init(&argc, &argv);

    builder = gtk_builder_new_from_file("./glade/window_main.glade");
    gtk_builder_connect_signals(builder, NULL);

    window = GTK_WIDGET(gtk_builder_get_object(builder, "window_main"));

    bttn_connect_database = GTK_WIDGET(gtk_builder_get_object(builder, "bttn_connect_database"));

    combo_tableNames = GTK_COMBO_BOX_TEXT(gtk_builder_get_object(builder, "combo_tableNames"));

    bttn_disconectDB = GTK_WIDGET(gtk_builder_get_object(builder, "bttn_disconectDB"));

    bttn_exportCSV = GTK_WIDGET(gtk_builder_get_object(builder, "bttn_exportCSV"));

    sinceLastExportTggl = GTK_TOGGLE_BUTTON(gtk_builder_get_object(builder, "sinceLastExportTggl"));

    machineSelector = GTK_COMBO_BOX_TEXT(gtk_builder_get_object(builder, "machineSelector"));

    passwordEntry = GTK_ENTRY(gtk_builder_get_object(builder, "passwordEntry"));


    
    g_object_unref(builder);

    gtk_widget_show(window);                
    gtk_main();
    
    return 0;
}

// called when window is closed
void on_window_main_destroy()
{
    disconnectFromDatabase();
    gtk_main_quit();
}

void on_bttn_connect_database_clicked(GtkButton *b){
    string ipaddresses[100] = {"localhost","10.123.123.1","10.123.123.2"};
    string paswd;
    paswd = gtk_entry_get_text(GTK_ENTRY(passwordEntry));
    string activeComboBox;
    activeComboBox = gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(machineSelector));
    connectToDatabase(activeComboBox,paswd);
    
    cout << "button yeeeeeah!" << endl;
}
void on_combo_tableNames_changed(GtkComboBoxText *b){
    cout << "combo changed yeeeeah!" << endl;
}

void on_bttn_disconectDB_clicked(GtkButton *b){
    disconnectFromDatabase();

}

void on_bttn_exportCSV_clicked(GtkButton *b){
    char date[50] = "2019-01-01 00:00:00.000";
    string activeComboBox;
    activeComboBox = gtk_combo_box_text_get_active_text (GTK_COMBO_BOX_TEXT(combo_tableNames));

    if (gtk_toggle_button_get_active(sinceLastExportTggl)){
        getLastExport(activeComboBox,date);
    }
    if(readRows(activeComboBox, date)){
        writeErrorLog(getNextId("eventLog"), activeComboBox + " Exported to CSV", 20);
    }
}

void on_sinceLastExportTggl_toggled(GtkToggleButton *b){
        cout << "heck yeah you did" << endl;
}

void on_machineSelector_changed(GtkComboBoxText *b){
    cout << "Take that world!" << endl;
}
void on_passwordEntry_editing_done(GtkEntry *b){
    cout << "Be the best you can be!" << endl;
}