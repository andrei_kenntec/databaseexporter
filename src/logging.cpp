#include "include/logging.h"

SQLHANDLE SQLEnvHandle = NULL;
SQLHANDLE SQLConnectionHandle = NULL;
SQLHANDLE SQLStatementHandle = NULL;

void showSQLError(unsigned int handleType, const SQLHANDLE &handle)
{
	SQLCHAR SQLState[1024];
	SQLCHAR message[1024];
	if (SQL_SUCCESS == SQLGetDiagRec(handleType, handle, 1, SQLState, NULL, message, 1024, NULL))
		// Returns the current values of multiple fields of a diagnostic record that contains error, warning, and status information
		cout << "SQL driver message: " << message << "\nSQL state: " << SQLState << "." << endl;
}

bool writeRow(unsigned int id, unsigned int layout, string layoutID, int fruitCount, bool trayPacked, string cycleTime, string fruitType, string machineID)
{
	char SQLAddRandomData[256];
	ostringstream SQLQueryFormation;

	SQLQueryFormation << "INSERT INTO TrayLog(id, layout, layoutID, fruitCount, trayPacked, cycleTime, fruitType, machineID) VALUES("
					  << id << " ,"
					  << layout << " ,\'"
					  << layoutID << "\' ,"
					  << fruitCount << " ,"
					  << trayPacked << " ,\'"
					  << cycleTime << "\' ,\'"
					  << fruitType << "\' ,\'"
					  << machineID << "\')";

	strncpy(SQLAddRandomData, SQLQueryFormation.str().c_str(), sizeof(SQLAddRandomData));
	//cout << SQLAddRandomData << endl;

	if (SQL_SUCCESS != SQLAllocHandle(SQL_HANDLE_STMT, SQLConnectionHandle, &SQLStatementHandle))
		// Allocates the statement
		showSQLError(SQL_HANDLE_STMT, SQLStatementHandle);
	//break;

	if (SQL_SUCCESS != SQLExecDirect(SQLStatementHandle, (SQLCHAR *)SQLAddRandomData, SQL_NTS))
	{
		// Executes a preparable statement
		showSQLError(SQL_HANDLE_STMT, SQLStatementHandle);
		cout << "WritingRowFailed" << endl;
		return 0;
		//break;
	}
	return 1;
}

bool connectToDatabase(string server , string paswd){

	char SQLAddRandomData[250];
	ostringstream SQLQueryFormation;
	
	SQLQueryFormation << "DRIVER={ODBC Driver 17 for SQL Server}; SERVER=" << server <<", 1433; DATABASE=FillerHead1; UID=SA; PWD=" << paswd << ";";
	strncpy(SQLAddRandomData, SQLQueryFormation.str().c_str(), sizeof(SQLAddRandomData));

	SQLRETURN retCode = 0;
	SQLCHAR* convertedVar = (unsigned char*)SQLAddRandomData;

	do
	{
		if (SQL_SUCCESS != SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &SQLEnvHandle))
			// Allocates the environment
			break;

		if (SQL_SUCCESS != SQLSetEnvAttr(SQLEnvHandle, SQL_ATTR_ODBC_VERSION, (SQLPOINTER)SQL_OV_ODBC3, 0))
			// Sets attributes that govern aspects of environments
			break;

		if (SQL_SUCCESS != SQLAllocHandle(SQL_HANDLE_DBC, SQLEnvHandle, &SQLConnectionHandle))
			// Allocates the connection
			break;

		if (SQL_SUCCESS != SQLSetConnectAttr(SQLConnectionHandle, SQL_LOGIN_TIMEOUT, (SQLPOINTER)5, 0))
			// Sets attributes that govern aspects of connections
			break;

		SQLCHAR retConString[1024]; // Conection string
		switch (SQLDriverConnect(SQLConnectionHandle, NULL, convertedVar, SQL_NTS, retConString, 1024, NULL, SQL_DRIVER_NOPROMPT))
		{
		// Establishes connections to a driver and a data source
		case SQL_SUCCESS:
			cout << "Connection to database Success" << endl;
			break;
		case SQL_SUCCESS_WITH_INFO:
			break;
		case SQL_NO_DATA_FOUND:
			showSQLError(SQL_HANDLE_DBC, SQLConnectionHandle);
			retCode = -1;
			break;
		case SQL_INVALID_HANDLE:
			showSQLError(SQL_HANDLE_DBC, SQLConnectionHandle);
			retCode = -1;
			break;
		case SQL_ERROR:
			showSQLError(SQL_HANDLE_DBC, SQLConnectionHandle);
			retCode = -1;
			break;
		default:
			break;
		}

		if (retCode == -1)
		{
			break;
			cout << "Connection to database Failed with error" << endl;
			return 0;
		}
	} while (0);
	return 1;
}

bool readRows(string tableName, string date)
{
	char SQLAddRandomData[256];
	ostringstream SQLQueryFormation;
	int exportCount = 0;
	auto ExportTime = std::chrono::system_clock::now();
	char timeCol[10];

	//where A.Date >= Convert(datetime, '2010-04-01' )
	if(tableName == "eventLog"){
			strcpy(timeCol, "eventTime");
    }else if(tableName == "TrayLog"){
		strcpy(timeCol, "packTime");
	}else if(tableName == "StatusLog"){
		strcpy(timeCol, "logTime");
	}

	SQLQueryFormation << "SELECT * FROM " << tableName << " WHERE  " << timeCol << " >= CONVERT(datetime, '" << date << "')";
	strncpy(SQLAddRandomData, SQLQueryFormation.str().c_str(), sizeof(SQLAddRandomData));

	std::ofstream myfile;

	if (SQL_SUCCESS != SQLAllocHandle(SQL_HANDLE_STMT, SQLConnectionHandle, &SQLStatementHandle))
	{
		cout << "error in :" << SQLAddRandomData << endl;
		showSQLError(SQL_HANDLE_STMT, SQLStatementHandle);
		return 0;
		// Allocates the statement
		//break;
	}
	if (SQL_SUCCESS != SQLExecDirect(SQLStatementHandle, (SQLCHAR *)SQLAddRandomData, SQL_NTS))
	{
		cout << "error in :" << SQLAddRandomData << endl;
		// Executes a preparable statement
		showSQLError(SQL_HANDLE_STMT, SQLStatementHandle);
		return 0;
		//break;
	}
	else
	{
		std::time_t export_time = std::chrono::system_clock::to_time_t(ExportTime);

		if (tableName == "TrayLog")
		{

			char filename[50];
			strcpy(filename, ctime(&export_time)); // copy string one into the result.
			strcat(filename, " TrayLog Export.csv");

			cout << "ReadingRows From Database" << endl;
			myfile.open(filename);
			myfile << "id,layout,layoutID,fruitCount,trayPacked,cycleTime,packTime,fruitType,machineId" << endl;

			while (SQLFetch(SQLStatementHandle) == SQL_SUCCESS)
			{
				int id = 0;
				int layout = 0;
				char layoutID[256] = {0};
				int fruitCount = 0;
				int trayPacked = 0;
				char cycleTime[256] = {0};
				SQL_TIMESTAMP_STRUCT packTime = {0};
				char fruitType[256] = {0};
				char machineId[256] = {0};
				// Fetches the next rowset of data from the result
				SQLGetData(SQLStatementHandle, 1, SQL_C_DEFAULT, &id, sizeof(id), NULL);
				SQLGetData(SQLStatementHandle, 2, SQL_C_DEFAULT, &layout, sizeof(layout), NULL);
				SQLGetData(SQLStatementHandle, 3, SQL_C_DEFAULT, &layoutID, sizeof(layoutID), NULL);
				SQLGetData(SQLStatementHandle, 4, SQL_C_DEFAULT, &fruitCount, sizeof(fruitCount), NULL);
				SQLGetData(SQLStatementHandle, 5, SQL_C_DEFAULT, &trayPacked, sizeof(trayPacked), NULL);
				SQLGetData(SQLStatementHandle, 6, SQL_C_CHAR, &cycleTime, sizeof(cycleTime), NULL);
				SQLGetData(SQLStatementHandle, 7, SQL_C_DEFAULT, &packTime, sizeof(packTime), NULL);
				SQLGetData(SQLStatementHandle, 8, SQL_C_DEFAULT, &fruitType, sizeof(fruitType), NULL);
				SQLGetData(SQLStatementHandle, 9, SQL_C_DEFAULT, &machineId, sizeof(machineId), NULL);
				// Retrieves data for a single column in the result set

				myfile << id << "," << layout << "," << layoutID << "," << fruitCount << "," << trayPacked << "," << cycleTime << "," << packTime.year << packTime.month << packTime.day << "-" << packTime.hour << ":" << packTime.minute << ":" << packTime.second << ":" << packTime.fraction
					   << "," << fruitType << "," << machineId << endl;

				exportCount++;
			}
			myfile.close();
			cout << "End of Database Rows" << endl;
			return 1;
		}
		else if (tableName == "StatusLog")
		{
			int id = 0;
			char logTime[256] = {0};
			char beltFullness[256] = {0};
			bool laneSensorStates = 0;
			double trayRate = 0;
			char inhibitTime[256] = {0};
			double pressure = 0;
			double vacuum = 0;
			char error[256] = {0};

			char filename[50];
			strcpy(filename, ctime(&export_time)); // copy string one into the result.
			strcat(filename, " StatusLog Export.csv");

			cout << "ReadingRows From Database" << endl;
			myfile.open(filename);
			myfile << "id,logTime,beltFullness,laneSensorStates,trayRate,inhibitTime,pressure,vacuum,error" << endl;

			while (SQLFetch(SQLStatementHandle) == SQL_SUCCESS)
			{
				// Fetches the next rowset of data from the result
				SQLGetData(SQLStatementHandle, 1, SQL_C_DEFAULT, &id, sizeof(id), NULL);
				SQLGetData(SQLStatementHandle, 2, SQL_C_DEFAULT, &logTime, sizeof(logTime), NULL);
				SQLGetData(SQLStatementHandle, 3, SQL_C_DEFAULT, &beltFullness, sizeof(beltFullness), NULL);
				SQLGetData(SQLStatementHandle, 4, SQL_C_DEFAULT, &laneSensorStates, sizeof(laneSensorStates), NULL);
				SQLGetData(SQLStatementHandle, 5, SQL_C_DEFAULT, &trayRate, sizeof(trayRate), NULL);
				SQLGetData(SQLStatementHandle, 6, SQL_C_CHAR, &inhibitTime, sizeof(inhibitTime), NULL);
				SQLGetData(SQLStatementHandle, 7, SQL_C_DEFAULT, &pressure, sizeof(pressure), NULL);
				SQLGetData(SQLStatementHandle, 8, SQL_C_DEFAULT, &vacuum, sizeof(vacuum), NULL);
				SQLGetData(SQLStatementHandle, 9, SQL_C_DEFAULT, &error, sizeof(error), NULL);
				// Retrieves data for a single column in the result set

				myfile << id << "," << logTime << "," << beltFullness << "," << laneSensorStates << "," << trayRate << "," << inhibitTime << "," << pressure << "," << vacuum << "," << error << endl;
			}
			myfile.close();
			cout << "End of Database Rows" << endl;
			return 1;
		}
		else if (tableName == "eventLog")
		{
			//id INT NOT NULL UNIQUE, eventTime DATETIME DEFAULT getdate(), description VARCHAR(100), eventId INT
			int id = 0;
			SQL_TIMESTAMP_STRUCT eventTime = {0};
			char description[256] = {0};
			int eventId = 0;

			char filename[50];
			strcpy(filename, ctime(&export_time)); // copy string one into the result.
			strcat(filename, " eventLog Export.csv");

			cout << "ReadingRows From Database" << endl;
			myfile.open(filename);
			myfile << "id,eventTime,description,eventId" << endl;

			while (SQLFetch(SQLStatementHandle) == SQL_SUCCESS)
			{
				// Fetches the next rowset of data from the result
				SQLGetData(SQLStatementHandle, 1, SQL_C_DEFAULT, &id, sizeof(id), NULL);
				SQLGetData(SQLStatementHandle, 2, SQL_C_DEFAULT, &eventTime, sizeof(eventTime), NULL);
				SQLGetData(SQLStatementHandle, 3, SQL_C_DEFAULT, &description, sizeof(description), NULL);
				SQLGetData(SQLStatementHandle, 4, SQL_C_DEFAULT, &eventId, sizeof(eventId), NULL);

				// Retrieves data for a single column in the result set

				myfile << id << "," << eventTime.year << eventTime.month << eventTime.day << "-" << eventTime.hour << ":" << eventTime.minute << ":" << eventTime.second << "." << eventTime.fraction << "," << description << "," << eventId << endl;
			}
			myfile.close();
			cout << "End of Database Rows" << endl;
			return 1;
		}
		else
		{
			cout << "what table is this one bro?" << endl;
			return 0;
		}
		cout << exportCount << " rows exported";
	}
	return 0;
}

void disconnectFromDatabase()
{
	SQLFreeHandle(SQL_HANDLE_STMT, SQLStatementHandle);
	SQLDisconnect(SQLConnectionHandle);
	SQLFreeHandle(SQL_HANDLE_DBC, SQLConnectionHandle);
	SQLFreeHandle(SQL_HANDLE_ENV, SQLEnvHandle);
	// Frees the resources and disconnects
	cout << "Disconnected From Database" << endl;
}

int getNextId(string tableName)
{
	char SQLAddRandomData[256];
	ostringstream SQLQueryFormation;
	int id;

	SQLQueryFormation << "SELECT MAX(id) FROM " << tableName;

	strncpy(SQLAddRandomData, SQLQueryFormation.str().c_str(), sizeof(SQLAddRandomData));
	cout << SQLAddRandomData << endl;

	if (SQL_SUCCESS != SQLAllocHandle(SQL_HANDLE_STMT, SQLConnectionHandle, &SQLStatementHandle))
	{
		cout << "Error in : " << SQLAddRandomData << endl;
		showSQLError(SQL_HANDLE_STMT, SQLStatementHandle);
		// Allocates the statement
		//break;
	}
	if (SQL_SUCCESS != SQLExecDirect(SQLStatementHandle, (SQLCHAR *)SQLAddRandomData, SQL_NTS))
	{
		// Executes a preparable statement
		cout << "Error in : " << SQLAddRandomData << endl;
		showSQLError(SQL_HANDLE_STMT, SQLStatementHandle);
		//break;
	}
	else
	{
		cout << "Reading Next ID" << endl;
		while (SQLFetch(SQLStatementHandle) == SQL_SUCCESS)
		{
			// Fetches the next rowset of data from the result
			SQLGetData(SQLStatementHandle, 1, SQL_C_DEFAULT, &id, sizeof(id), NULL);
			// Retrieves data for a single column in the result set
			cout << "Next id is: " << id + 1 << endl;
		}
	}

	return id + 1;
}

double getRateOfTrays(int minutes)
{

	char SQLAddRandomData[256];
	ostringstream SQLQueryFormation;
	int rowID = 0;
	int count = 0;
	double rate = 0.0;

	//SQLQueryFormation << "SELECT * FROM TrayLog WHERE packTime > DATEADD(mi,-" << minutes << ",GetDate())";

	strncpy(SQLAddRandomData, SQLQueryFormation.str().c_str(), sizeof(SQLAddRandomData));
	cout << SQLAddRandomData << endl;
	//char SQLQuery[256] = SQLAddRandomData;

	if (SQL_SUCCESS != SQLAllocHandle(SQL_HANDLE_STMT, SQLConnectionHandle, &SQLStatementHandle))
		showSQLError(SQL_HANDLE_STMT, SQLStatementHandle);
	// Allocates the statement
	//break;

	if (SQL_SUCCESS != SQLExecDirect(SQLStatementHandle, (SQLCHAR *)SQLAddRandomData, SQL_NTS))
	{
		// Executes a preparable statement
		showSQLError(SQL_HANDLE_STMT, SQLStatementHandle);
		//break;
	}
	else
	{
		cout << "Counting last " << minutes << " Minutes of database Entries" << endl;
		while (SQLFetch(SQLStatementHandle) == SQL_SUCCESS)
		{
			SQLGetData(SQLStatementHandle, 1, SQL_C_DEFAULT, &rowID, sizeof(rowID), NULL);
			count++;
		}
		cout << count << " counted" << endl;
	}

	rate = count / minutes;
	return rate;
}

bool writeStatusUpdate(unsigned int id, double beltFullness, unsigned int laneSensorStates,
					   double trayRate, string inhibitTime, double pressure, double vacuum, string error)
{
	char SQLAddRandomData[256];
	ostringstream SQLQueryFormation;

	SQLQueryFormation << "INSERT INTO StatusLog(id, beltFullness, laneSensorStates, trayRate, inhibitTime, pressure, vacuum, error) VALUES("
					  << id << " ,"
					  << beltFullness << " ,"
					  << laneSensorStates << " ,"
					  << trayRate << " ,\'"
					  << inhibitTime << "\' ,"
					  << pressure << " ,"
					  << vacuum << ",\'"
					  << error << "\')";

	strncpy(SQLAddRandomData, SQLQueryFormation.str().c_str(), sizeof(SQLAddRandomData));
	//cout << SQLAddRandomData << endl;

	if (SQL_SUCCESS != SQLAllocHandle(SQL_HANDLE_STMT, SQLConnectionHandle, &SQLStatementHandle))
	{
		// Allocates the statement
		cout << "Error in : " << SQLAddRandomData << endl;
		showSQLError(SQL_HANDLE_STMT, SQLStatementHandle);
		//break;
	}
	if (SQL_SUCCESS != SQLExecDirect(SQLStatementHandle, (SQLCHAR *)SQLAddRandomData, SQL_NTS))
	{
		// Executes a preparable statement
		cout << "Error in : " << SQLAddRandomData << endl;
		showSQLError(SQL_HANDLE_STMT, SQLStatementHandle);
		cout << "WritingRowFailed" << endl;
		return 0;
		//break;
	}
	return 1;
}

bool writeErrorLog(unsigned int id, string description, int eventID)
{
	char SQLAddRandomData[256];
	ostringstream SQLQueryFormation;

	SQLQueryFormation << "INSERT INTO eventLog(id, description, eventID) VALUES("
					  << id << " ,\'"
					  << description << "\' ,"
					  << eventID << ")";

	strncpy(SQLAddRandomData, SQLQueryFormation.str().c_str(), sizeof(SQLAddRandomData));
	//cout << SQLAddRandomData << endl;

	if (SQL_SUCCESS != SQLAllocHandle(SQL_HANDLE_STMT, SQLConnectionHandle, &SQLStatementHandle))
	{
		// Allocates the statement
		cout << "Error in : " << SQLAddRandomData << endl;
		showSQLError(SQL_HANDLE_STMT, SQLStatementHandle);
		//break;
	}
	if (SQL_SUCCESS != SQLExecDirect(SQLStatementHandle, (SQLCHAR *)SQLAddRandomData, SQL_NTS))
	{
		// Executes a preparable statement
		cout << "Error in : " << SQLAddRandomData << endl;
		showSQLError(SQL_HANDLE_STMT, SQLStatementHandle);
		cout << "WritingRowFailed" << endl;
		return 0;
		//break;
	}
	cout << "Event : " << description << "Logged" << endl;
	return 1;
}

bool tableExists(string tableName)
{
	char SQLAddRandomData[256];
	ostringstream SQLQueryFormation;

	SQLQueryFormation << "SELECT * FROM information_schema.tables WHERE table_name = \'" << tableName << "\' LIMIT 1)";

	strncpy(SQLAddRandomData, SQLQueryFormation.str().c_str(), sizeof(SQLAddRandomData));
	//cout << SQLAddRandomData << endl;

	if (SQL_SUCCESS != SQLAllocHandle(SQL_HANDLE_STMT, SQLConnectionHandle, &SQLStatementHandle))
	{
		// Allocates the statement
		cout << "error in :" << SQLAddRandomData << endl;
		showSQLError(SQL_HANDLE_STMT, SQLStatementHandle);
		//break;
	}
	if (SQL_SUCCESS != SQLExecDirect(SQLStatementHandle, (SQLCHAR *)SQLAddRandomData, SQL_NTS))
	{
		// Executes a preparable statement
		cout << "error in :" << SQLAddRandomData << endl;
		showSQLError(SQL_HANDLE_STMT, SQLStatementHandle);
		cout << "Table doesn't exist" << endl;
		return 0;
		//break;
	}
	return 1;
}
bool createTrayLog()
{

	char SQLAddRandomData[256];
	ostringstream SQLQueryFormation;

	SQLQueryFormation << "CREATE TABLE TrayLog (id INT NOT NULL UNIQUE, layout INT, layoutID VARCHAR(100), fruitCount INT, trayPacked INT, cycleTime TIME, packTime DATETIME DEFAULT getdate(), trayImageCo IMAGE, trayImageDep IMAGE, fruitType VARCHAR(100), machineID VARCHAR(100))";

	strncpy(SQLAddRandomData, SQLQueryFormation.str().c_str(), sizeof(SQLAddRandomData));
	//cout << SQLAddRandomData << endl;

	if (SQL_SUCCESS != SQLAllocHandle(SQL_HANDLE_STMT, SQLConnectionHandle, &SQLStatementHandle))
		// Allocates the statement
		showSQLError(SQL_HANDLE_STMT, SQLStatementHandle);
	//break;

	if (SQL_SUCCESS != SQLExecDirect(SQLStatementHandle, (SQLCHAR *)SQLAddRandomData, SQL_NTS))
	{
		// Executes a preparable statement
		showSQLError(SQL_HANDLE_STMT, SQLStatementHandle);
		cout << "create Failed" << endl;
		return 0;
		//break;
	}
	writeRow(1, 1, "NULL", 1, 1, "00:00:01", "NULL", "NULL"); //Write a initializing value into the table
	cout << "Table Created" << endl;
	return 1;
}
bool createStatusTable()
{

	char SQLAddRandomData[256];
	ostringstream SQLQueryFormation;

	SQLQueryFormation << "CREATE TABLE StatusLog (id INT NOT NULL UNIQUE, logTime DATETIME DEFAULT getdate(), beltFullness FLOAT, laneSensorStates BIT, trayRate FLOAT, inhibitTime TIME, pressure FLOAT, vacuum FLOAT, error VARCHAR(100))";

	strncpy(SQLAddRandomData, SQLQueryFormation.str().c_str(), sizeof(SQLAddRandomData));
	//cout << SQLAddRandomData << endl;

	if (SQL_SUCCESS != SQLAllocHandle(SQL_HANDLE_STMT, SQLConnectionHandle, &SQLStatementHandle))
		// Allocates the statement
		showSQLError(SQL_HANDLE_STMT, SQLStatementHandle);
	//break;

	if (SQL_SUCCESS != SQLExecDirect(SQLStatementHandle, (SQLCHAR *)SQLAddRandomData, SQL_NTS))
	{
		// Executes a preparable statement
		showSQLError(SQL_HANDLE_STMT, SQLStatementHandle);
		cout << "createFailed" << endl;
		return 0;
		//break;
	}
	writeStatusUpdate(1, 1.0, 1, 1.0, "00:00:01", 1.0, 1.0, "NULL");
	cout << "Table Created" << endl;
	return 1;
}

bool createErrorTable()
{
	cout << "creating eventlog" << endl;

	char SQLAddRandomData[256];
	ostringstream SQLQueryFormation;

	SQLQueryFormation << "CREATE TABLE eventLog (id INT NOT NULL UNIQUE, eventTime DATETIME DEFAULT getdate(), description VARCHAR(100), eventId INT)";

	strncpy(SQLAddRandomData, SQLQueryFormation.str().c_str(), sizeof(SQLAddRandomData));
	//cout << SQLAddRandomData << endl;

	if (SQL_SUCCESS != SQLAllocHandle(SQL_HANDLE_STMT, SQLConnectionHandle, &SQLStatementHandle))
		// Allocates the statement
		showSQLError(SQL_HANDLE_STMT, SQLStatementHandle);
	//break;

	if (SQL_SUCCESS != SQLExecDirect(SQLStatementHandle, (SQLCHAR *)SQLAddRandomData, SQL_NTS))
	{
		// Executes a preparable statement
		showSQLError(SQL_HANDLE_STMT, SQLStatementHandle);
		cout << "CreateDatabaseFailed" << endl;
		return 0;
		//break;
	}
	writeErrorLog(1, "NULL", 1);
	cout << "Table Created" << endl;
	return 1;
}

bool getLastExport(string tableName, char* date)
{

	char SQLAddRandomData[256];
	ostringstream SQLQueryFormation;

	SQLQueryFormation << "SELECT MAX(eventTime) FROM eventLog WHERE description LIKE '" << tableName << " Exported to CSV'";
	strncpy(SQLAddRandomData, SQLQueryFormation.str().c_str(), sizeof(SQLAddRandomData));

	std::ofstream myfile;

	if (SQL_SUCCESS != SQLAllocHandle(SQL_HANDLE_STMT, SQLConnectionHandle, &SQLStatementHandle))
	{
		cout << "error in :" << SQLAddRandomData << endl;
		showSQLError(SQL_HANDLE_STMT, SQLStatementHandle);
		// Allocates the statement
		//break;
		return 0;
	}
	if (SQL_SUCCESS != SQLExecDirect(SQLStatementHandle, (SQLCHAR *)SQLAddRandomData, SQL_NTS))
	{
		cout << "error in :" << SQLAddRandomData << endl;
		// Executes a preparable statement
		showSQLError(SQL_HANDLE_STMT, SQLStatementHandle);
		//break;
		return 0;
	}
	else
	{
	char lastTime[30];

			while (SQLFetch(SQLStatementHandle) == SQL_SUCCESS)
		{
			// Fetches the next rowset of data from the result
			SQLGetData(SQLStatementHandle, 1, SQL_C_CHAR, &lastTime, sizeof(lastTime), NULL);
			// Retrieves data for a single column in the result set
			strcpy(date,lastTime);
			cout << "The last Export was at: " << lastTime << endl;
		}
	}
	return 1;
}