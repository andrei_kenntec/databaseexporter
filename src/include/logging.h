#include <cstddef>
#include <string>
#include <cstring>
#include <sstream>
#include <iostream>
#include <fstream>
#include <chrono>
#include <ctime>
#include </usr/include/sql.h>
#include </usr/include/sqlext.h>
#include </usr/include/sqltypes.h>
#include </usr/include/sqlucode.h>

using namespace std;


void showSQLError(unsigned int handleType, const SQLHANDLE& handle);
bool writeRow(unsigned int id,unsigned int layout, string layoutID,int fruitCount,
                bool trayPacked,string cycleTime,string fruitType, string machineID);
bool connectToDatabase(string server , string paswd);
bool readRows(string tableName, string date);
void disconnectFromDatabase();
int getNextId(string tableName);
double getRateOfTrays(int minutes);
bool writeStatusUpdate(unsigned int id , double beltFullness,unsigned int laneSensorStates, 
                        double trayRate,string inhibitTime,double pressure,double vacuum,string error);
bool writeErrorLog(unsigned int id , string description, int eventID);
bool tableExists();

bool createTrayLog();
bool createStatusTable();
bool createErrorTable();

bool getLastExport(string tableName, char* date);
