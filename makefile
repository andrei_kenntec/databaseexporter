#g++ main.cpp coms.cpp roi.cpp vision.cpp sys_config_vis.cpp logging.cpp 
#../AdsLib-Linux.a -lpthread -std=c++11 -I../AdsLib/ -I../include/ -lrealsense2 
#-lodbc `pkg-config --cflags --libs opencv` `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0` -o main.bin


# change application name here (executable output name)
TARGET=database_export

# compiler
CXX=g++
# debug
DEBUG=-g
# optimisation
OPT=-O0
# warnings
WARN=-Wall

PTHREAD=-pthread

ODBC = -lodbc

CXXFLAGS=$(DEBUG) $(OPT) $(WARN) $(PTHREAD) -std=c++11 -pipe 


GTKLIB= `pkg-config --cflags --libs gtk+-3.0`

# linker
LD=g++
LDFLAGS=$(PTHREAD) $(ODBC) $(GTKLIB) -export-dynamic

OBJS=    main.o logging.o

all: $(OBJS)
	$(LD) -o $(TARGET) $(OBJS) $(LDFLAGS)
    
main.o: src/main.cpp
	$(CXX) -c $(CXXFLAGS) src/main.cpp $(GTKLIB) -o main.o

logging.o: src/logging.cpp
	$(CXX) -c $(CXXFLAGS) src/logging.cpp -o logging.o
    
clean:
	rm -f *.o $(TARGET)
